import { Component, Input, OnInit } from '@angular/core';

import { ApiService } from './../api/api.service';
import { Information } from './../models/information.model';
import { Anecdote } from './../models/anecdote.model';
import { User } from './../models/user.model';
import { LoginServiceService } from '../loginService/login-service.service';
import { AuthorizationService } from '../service/authorization.service';


@Component({
  selector: 'app-restapi',
  templateUrl: './restapi.component.html',
  styleUrls: ['./restapi.component.css']
})
export class RestapiComponent implements OnInit {

  constructor(
    private api: ApiService,
    public loginService: LoginServiceService,
    public auth: AuthorizationService) { }
    
  @Input() wordSearched: string = '';

  isAuthInComponentRestApi: boolean = false;

  isFirstPreferenceAvailable: boolean = false;
  isSecondPreferenceAvailable: boolean = false;
  isThirdPreferenceAvailable: boolean = false;
  isFourthPreferenceAvailable: boolean = false;
  isFifthPreferenceAvailable: boolean = false;
  isSixthPreferenceAvailable: boolean = false;

  cognitoUserConnectedPreferencesInRestApiOne: string = this.auth.cognitoUserConnectedPreferences[0];
  cognitoUserConnectedPreferencesInRestApiTwo = this.auth.cognitoUserConnectedPreferences[1];
  cognitoUserConnectedPreferencesInRestApiThree = this.auth.cognitoUserConnectedPreferences[2];
  cognitoUserConnectedPreferencesInRestApiFour = this.auth.cognitoUserConnectedPreferences[3];
  cognitoUserConnectedPreferencesInRestApiFive = this.auth.cognitoUserConnectedPreferences[4];
  cognitoUserConnectedPreferencesInRestApiSix = this.auth.cognitoUserConnectedPreferences[5];

  _informations: Information[] = [];
  _anecdotes: Anecdote[] = [];
  _anecdotesTwo: Anecdote[] = [];
  _anecdotesByPreferencesOne: Anecdote[] = [];
  _users: User[] = [];



  ngOnInit(): void {
    setTimeout(() => {
      this.getInformations()
      this.getAnecdotesByDescriptionContent(this.cognitoUserConnectedPreferencesInRestApiOne);
    }, 1000)

    if (this.cognitoUserConnectedPreferencesInRestApiOne.length > 2){
      this.isFirstPreferenceAvailable = true;
    } if (this.cognitoUserConnectedPreferencesInRestApiTwo.length > 2){
      this.isSecondPreferenceAvailable = true;
    } if (this.cognitoUserConnectedPreferencesInRestApiThree.length > 2){
      this.isThirdPreferenceAvailable = true;
    } if (this.cognitoUserConnectedPreferencesInRestApiFour.length > 2){
      this.isFourthPreferenceAvailable = true;
    } if (this.cognitoUserConnectedPreferencesInRestApiFive.length > 2){
      this.isFifthPreferenceAvailable = true;
    } if (this.cognitoUserConnectedPreferencesInRestApiSix.length > 2){
      this.isThirdPreferenceAvailable = true;
    }

  }


  onClickSearchWord() {
    console.log("Word searched : '" + this.wordSearched + "'");
    this.getAnecdotesByDescriptionContent(this.wordSearched);
    console.log("User : " + this.loginService.connexionId);
    console.log("Is authenticated : " + this.loginService.isAuth);
    this.isAuthInComponentRestApi = this.loginService.isAuth;
    console.log("Adresse mail : " + this.loginService.email);
  }
  onClickSearchPreferenceOne() {
    this.getAnecdotesByDescriptionContent(this.cognitoUserConnectedPreferencesInRestApiOne);
  }
  onClickSearchPreferenceTwo() {
    this.getAnecdotesByDescriptionContent(this.cognitoUserConnectedPreferencesInRestApiTwo);
  }
  onClickSearchPreferenceThree() {
    this.getAnecdotesByDescriptionContent(this.cognitoUserConnectedPreferencesInRestApiThree);
  }
  onClickSearchPreferenceFour() {
    this.getAnecdotesByDescriptionContent(this.cognitoUserConnectedPreferencesInRestApiFour);
  }



  getInformations() {
    this.api.getInformations()
      .subscribe(
        (data) => { this._informations = data }
      );
  }

  getAnecdotes() {
    this.api.getAnecdotes()
      .subscribe(
        (data) => { this._anecdotes = data }
      );
  }

  getAnecdotesByDescriptionContent(descriptionContent: string) {
    this.api.getAnecdotesByDescriptionContent(descriptionContent)
      .subscribe(
        (data) => { this._anecdotes = data }
      );
  }
  getAnecdotesByDescriptionContentTwo(descriptionContent: string) {
    this.api.getAnecdotesByDescriptionContent(descriptionContent)
      .subscribe(
        (data) => { this._anecdotesTwo = data }
      );
  }

  getUsers() {
    this.api.getUsers()
      .subscribe(
        (data) => { this._users = data }
      );
  }

  getUserById(id: string) {
    this.api.getUserById(id)
      .subscribe(
        (data) => { this._users = data }
      );
  }

}



// ==================================================================================================================================================
// Cours Angular :                                                                                                                                  |
// https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular/5088271-gerez-des-donnees-dynamiques#/id/r-5141363    |
// ==================================================================================================================================================
