import { Component, OnInit, Input } from '@angular/core';
import { AuthorizationService } from '../service/authorization.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ApiService } from './../api/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  codeToConfirm: boolean = false;
  codeWasConfirmed: boolean = false;
  error: string = "";

  @Input() emailRegistered: string = '';
  @Input() passwordRegistered: string = '';
  @Input() confirmCode: string = '';

  constructor(private auth: AuthorizationService,
              private _router: Router,
              private api: ApiService
              ) { }

  register() {
    const email = this.emailRegistered;
    const password = this.passwordRegistered;
    
    // ---- TEST ----
    // this.api.addUser(email);
    //---------------

    //  A remettre en place après les tests
    this.auth.register(email, password).subscribe(
      (data) => {
        this.codeToConfirm = true;
      },
      (err) => {
        console.log(err);
        this.error = "Registration Error has occured";
      }
    );


    // console.log(email);
    // console.log(password);
  }

  validateAuthCode() {
    const code = this.confirmCode;
    const email = this.emailRegistered;

    this.auth.confirmAuthCode(code).subscribe( 
      (data: any) => {
        // this._router.navigateByUrl('/');
        this.codeWasConfirmed = true;
        this.codeToConfirm = false;

        this.api.addUser(email);
      },
      (err: any) => {
        console.log(err);
        this.error = "Confirm Authorization Error has occured";
      }
    )
  }

  ngOnInit(): void {
  }

}
