import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../api/api.service';
import { User } from '../models/user.model';
import { LoginServiceService } from '../loginService/login-service.service';
import { NgForm } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { AuthorizationService } from '../service/authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() emailFilled: string = '';
  @Input() passwordFilled: string = '';

  @Input() connexionIdUpdated: string = '';
  @Input() firstNameUpdated: string = '';
  @Input() lastNameUpdated: string = '';
  @Input() passwordUpdated: string = '';
  @Input() preferencesUpdatedOne: string = '';
  @Input() preferencesUpdatedTwo: string = '';
  @Input() preferencesUpdatedThree: string = '';
  @Input() preferencesUpdatedFour: string = '';
  @Input() emailUpdated: string = '';

  userConnectedInLoginEmail: string = '';
  userConnectedInLoginFirstname: string = '';
  userConnectedInLoginLastname: string = '';
  userConnectedInLoginPreferences: string[] = [];
  userConnectedInLoginId: string = '';

  lastNameTopUpdateInDb: any;
  firstNameTopUpdateInDb: any;
  preferencesTopUpdateInDb: string[] = [];

  isAuthInLoginComponent: boolean = false;
  isAuthInLoginComponentUpdateMode: boolean = false;
  alertConnexionMessage: boolean = false;

  _users: User[] = [];

  emailVerificationMessage: boolean = false;

  constructor(private api: ApiService,
    public loginService: LoginServiceService,
    public auth: AuthorizationService,
    private _router: Router) { }

  onSubmit() {
    var email = this.emailFilled;
    var password = this.passwordFilled;

    this.auth.signIn(email, password).subscribe((data: any) => {

      this.auth.cognitoUserMail = email;
      this.auth.isAuthInAuthorizationService = true;
      console.log("User connected : " + this.auth.cognitoUserMail);
      this.isAuthInLoginComponentUpdateMode = true;

      // this._router.navigateByUrl('/');

      this.api.getUsers()
        .subscribe(
          (data) => {
            this._users = data
            for (const user of this._users) {
              if (user.email == this.emailFilled) {
                // console.log(user.connexionId)
                // console.log(user.email);
                // console.log(user.firstName);
                // console.log(user.lastName);
                // console.log(user.preferences);
                this.userConnectedInLoginEmail = user.email;
                this.auth.cognitoUserConnectedEmail = user.email;

                this.userConnectedInLoginFirstname = user.firstName;
                this.auth.cognitoUserConnectedFirstname = user.firstName;

                this.userConnectedInLoginLastname = user.lastName;
                this.auth.cognitoUserConnectedLastname = user.lastName;

                this.userConnectedInLoginPreferences = user.preferences;
                this.auth.cognitoUserConnectedPreferences = user.preferences;

                this.userConnectedInLoginId = user.id;
                this.auth.cognitoUserConnectedId = user.id;
              }
            }
            if (this.userConnectedInLoginEmail == '') { console.log("Aucune correspondance de cette adresse avec les adresses mails en base de donnée") }
          }
        );

    }, (err: any) => {
      this.emailVerificationMessage = true;
    });

    // console.log(email);
    // console.log(password);
  }

  onConnect() {
    var email = this.emailFilled;
    var password = this.passwordFilled;

    this.auth.signIn(email, password).subscribe((data: any) => {

      this.auth.cognitoUserMail = email;
      this.auth.isAuthInAuthorizationService = true;
      console.log("User connected : " + this.auth.cognitoUserMail);
      this.isAuthInLoginComponent = true;

      // this._router.navigateByUrl('/');

      this.api.getUsers()
        .subscribe(
          (data) => {
            this._users = data
            for (const user of this._users) {
              if (user.email == this.emailFilled) {

                this.userConnectedInLoginEmail = user.email;
                this.auth.cognitoUserConnectedEmail = user.email;

                this.userConnectedInLoginFirstname = user.firstName;
                this.auth.cognitoUserConnectedFirstname = user.firstName;

                this.userConnectedInLoginLastname = user.lastName;
                this.auth.cognitoUserConnectedLastname = user.lastName;

                this.userConnectedInLoginPreferences = user.preferences;
                this.auth.cognitoUserConnectedPreferences = user.preferences;

                this.userConnectedInLoginId = user.id;
                this.auth.cognitoUserConnectedId = user.id;
              }
            }
            if (this.userConnectedInLoginEmail == '') { console.log("Aucune correspondance de cette adresse avec les adresses mails en base de donnée") }
          }
        );

    }, (err: any) => {
    });

    // setTimeout(() => {
    //   this._router.navigateByUrl('/restapi')
    // }, 1000)
  }

  ngOnInit(): void {
  }

  onClickUpdateButton() {
    this.preferencesTopUpdateInDb.push(this.preferencesUpdatedOne, this.preferencesUpdatedTwo, this.preferencesUpdatedThree, this.preferencesUpdatedFour)

    // Amélioration de l'update car à ce stade il est impératif d'avoir 4 préf/mots clés personnels
    this.api.updateUser(
      this.userConnectedInLoginId,
      this.userConnectedInLoginEmail,
      this.lastNameUpdated,
      this.firstNameUpdated,
      this.preferencesTopUpdateInDb);

    this.alertConnexionMessage = true;
  }

  onClickDeleteButton() {
    this.api.deleteUser(this.userConnectedInLoginId);
    console.log("Suppression du compte afférent à l'id " + this.userConnectedInLoginId)
  }

  onClickConnectAfterUpdate() {
    window.location.reload();
  }

  onClickConnexion() {
    console.log("Tentative de connexion de la part de " + this.emailFilled + ", mot de passe : " + this.passwordFilled);
    this.api.getUsers()
      .subscribe(
        (data) => { this._users = data }
      );
    for (const user of this._users) {
      console.log("Connexion success !");
      console.log("User : " + user.connexionId)
      this.loginService.isAuth = true;
      this.loginService.id = user.id;
      console.log(this.loginService.id);
      this.loginService.connexionId = user.connexionId;
      console.log(this.loginService.connexionId);
      this.loginService.password = user.password;
      console.log(this.loginService.password);
      this.loginService.lastName = user.lastName;
      console.log(this.loginService.lastName);
      this.loginService.firstName = user.firstName;
      console.log(this.loginService.firstName);
      this.loginService.email = user.email;
      console.log(this.loginService.email);
      this.loginService.preferences = user.preferences;
      console.log(this.loginService.preferences);
      console.log("Is authenticated : " + this.loginService.isAuth);
      this.isAuthInLoginComponent = true;
      break;
    }
  }

}

