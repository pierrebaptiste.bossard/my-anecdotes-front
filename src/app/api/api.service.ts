import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { environment } from './../../environments/environment';
import { Anecdote } from './../models/anecdote.model';
import { Information } from './../models/information.model';
import { User } from '../models/user.model';
import { stringify } from '@angular/compiler/src/util';

// URL for api
const API_NODEJS_URL = environment.apiNodejsUrl;
const API_SPRING_URL = environment.apiSpringUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  postId: string = '';
  errorMessage: any;

  constructor(private http: HttpClient) { }

  //====   Requêtes sur API NodeJS   ============
  getInformations(): Observable<Information[]> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.get<Information[]>(API_NODEJS_URL + '/informations', { headers }).pipe(catchError(this.handleError));
  }

  getAnecdotes(): Observable<Anecdote[]> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.get<Anecdote[]>(API_NODEJS_URL + '/anecdotes', { headers }).pipe(catchError(this.handleError));
  }

  getAnecdotesByDescriptionContent(descriptionContent: string): Observable<Anecdote[]> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.get<Anecdote[]>(API_NODEJS_URL + '/anecdotesByDescriptionContent/' + descriptionContent, { headers }).pipe(catchError(this.handleError));
  }
  //=============================================


  //====   Requêtes sur API Spring   ============
  getUsers(): Observable<User[]> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.get<User[]>(API_SPRING_URL + '/map/users', { headers }).pipe(catchError(this.handleError));
  }
  getUserById(id: string): Observable<User[]> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.get<User[]>(API_SPRING_URL + '/map/user/' + id, { headers }).pipe(catchError(this.handleError));
  }
  updateUser(id: string, email: string, lastNameModified: string, firstNameModified: string, preferencesModified: string[]) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    const body = {
      id: id,
      connexionId: "defaultId",
      password: "defaultPassword",
      lastName: lastNameModified,
      firstName: firstNameModified,
      email: email,
      preferences: preferencesModified
    };
    this.http.put<User>(API_SPRING_URL + '/map/updateUser', body, { headers })
      .pipe(catchError(this.handleError))
      .subscribe({
        next: data => {
          this.postId = data.id;
        },
        error: error => {
          this.errorMessage = error.message;
          console.error("Il y'a une erreur ! ", error);
        }
      });
  }

  addUser(email: string) {
    // const headers = new HttpHeaders().set("Content-Type", "application/json");
    const defaultPreferences = ["Mot clé n°1, à modifier", "Mot clé n°2, à modifier"];

    const body = {
        connexionId: "Veuillez renseigner un Id de connexion",
        password: "Veuillez renseigner un mot de passe", // Non utilisé avec l'implémentation de Cognito
        lastName: "Veuillez renseigner votre nom de famille",
        firstName: "Veuillez renseigner votre prénom",
        email: email,
        preferences: defaultPreferences
    };
    // this.http.post<User>('https://spring-gitlab-env-staging.herokuapp.com/map/addUser', body, { headers })
    //     .pipe( catchError(this.handleError))
    //     .subscribe({
    //       next: data => {
    //       },
    //       error: error => {
    //         this.errorMessage = error.message;
    //         console.error("Il y'a une erreur ! ", error);
    //       }
    //     });
    this.http.post<any>(API_SPRING_URL + '/map/addUser', body)
      .subscribe({
        next: data => {
          this.postId = data.id;
        },
        error: error => {
          this.errorMessage = error.message;
          console.error("Il y'a une erreur ! ", error);
        }
      });
  }



  deleteUser(id: string) {
    this.http.delete<any>(API_SPRING_URL + '/map/deleteUser/' + id)
    .subscribe({
      next: data => {
        this.postId = data.id;
      },
      error: error => {
        this.errorMessage = error.message;
        console.error("Il y'a une erreur ! ", error);
      }
    });

  }

  //=============================================

  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
}