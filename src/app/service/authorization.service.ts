import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';

const poolData = {
  UserPoolId: 'eu-west-3_Bepqij9qA',
  ClientId: 'hlg55gogf50nulvgkjqvckvrf'
};

const userPool = new CognitoUserPool(poolData);

// Constante contenant l'email du CognitoUser return par la fonction Login


@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  cognitoUser: any;
  cognitoUserMail: string = '';
  isAuthInAuthorizationService: boolean = false;
  
  cognitoUserConnectedEmail: string = '';
  cognitoUserConnectedFirstname: string = '';
  cognitoUserConnectedLastname: string = '';
  cognitoUserConnectedPreferences: string[] = [];
  cognitoUserConnectedId: string = '';

  constructor() { }

  register(email, password) {

    const attributeList = [];

    return Observable.create(observer => {
      userPool.signUp(email, password, attributeList, null, (err, result) => {
        if (err) {
          console.log(JSON.stringify(err));
          observer.error(err);
          // Faire ici la création d'instance dans la DB avec le mail
        }

        this.cognitoUser = result.user;
        console.log("signUp success", result);
        observer.next(result);
        observer.complete();
      })
    })
  }

  signIn(email, password) {

    const authenticationData = {
      Username : email,
      Password : password,
    };
    const authenticationDetails = new AuthenticationDetails(authenticationData);
    
    // A améliorer avec currentAuthenticatedUser().getEmailAdress (cf. doc Cognito)
    this.cognitoUserConnectedEmail = authenticationData.Username;

    const userData = {
      Username : email,
      Pool : userPool
    };

    const cognitoUser = new CognitoUser(userData);

    return Observable.create(observer => {

      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {

          // console.log(result);
          observer.next(result);
          observer.complete();
        },
        onFailure: function(err) {
          console.log(err);
          observer.error(err);
        },
      });
    });
   }

  isLoggedin() {
    return userPool.getCurrentUser() != null;
   }

  confirmAuthCode (code) {
    const user = {
      Username : this.cognitoUser.username,
      Pool : userPool
    };
    return Observable.create(observer => {
      const cognitoUser = new CognitoUser(user);

      cognitoUser.confirmRegistration(code, true, function(err, result) {
        if (err) {
          console.log(err);
          observer.error(err);
        }

        console.log("confirmAuthCode() success", result);
        observer.next(result);
        observer.complete();
      });
    });
   }

  getAuthenticatedUser() {
    return userPool.getCurrentUser();
   }

  logOut() {
    this.getAuthenticatedUser().signOut();
    this.cognitoUser = null;
  }

}
